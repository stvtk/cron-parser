const Parser = require('./helpers/parser');

const [cronExpression] = process.argv.slice(2, 3);

if (!cronExpression) {
  throw new Error('The cron expression is missing.');
}

const cronArguments = cronExpression.split(' ');

if (cronArguments.length !== 6) {
  throw new Error('The cron expression is invalid.');
}

const [minute, hour, dayOfMonth, month, dayOfWeek, command] = cronArguments;

const cronResults = Parser.parse({
  minute, hour, dayOfMonth, month, dayOfWeek,
});
cronResults.command = command;

Object.keys(cronResults).forEach((type) => {
  console.log(`${type}: ${cronResults[type]}`);
});
