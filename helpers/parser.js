const {
  pathEq, cond, path, test, pipe,
} = require('ramda');

const CRON_EXPRESSION_RANGE = {
  minute: {
    start: 0,
    end: 59,
  },
  hour: {
    start: 0,
    end: 24,
  },
  dayOfMonth: {
    start: 0,
    end: 31,
  },
  month: {
    start: 0,
    end: 12,
  },
  dayOfWeek: {
    start: 0,
    end: 7,
  },
};

const getPossibleValues = ({ start, end }) => {
  const values = [];
  for (let range = start; range <= end; range++) {
    values.push(range);
  }
  return values.join(' ');
};

const CRON_TYPES_PARSER = cond([
  [pathEq(['pattern'], '*'), getPossibleValues],
  [pipe(path(['pattern']), test(/^[0-9]+-[0-9]+$/)), ({ pattern, start, end }) => {
    const [low, max] = pattern.split('-');

    if (!low || !max) throw new Error(`The following expression is invalid:  ${pattern}`);
    if (low > max || low < start || max > end) throw new Error(`Range out of bound:  ${pattern}`);

    return getPossibleValues({ start: low, end: max });
  }],
  [pipe(path(['pattern']), test(/^[0-9]+(,[0-9]+)*$/)), ({ pattern, start, end }) => {
    const parts = pattern.split(',');
    const values = [];
    parts.forEach((part) => {
      if (part < start || part > end) throw new Error(`Range out of bound: ${part}`);
      values.push(part);
    });

    return values.join(' ');
  }],
  [pipe(path(['pattern']), test(/^\*|[0-9]+\/[0-9]+$/)), ({ pattern, start, end }) => {
    let [low, increment] = pattern.split('/');

    // for * we set it to the allowed min
    if (low === '*') low = start;
    if (low < start || start > end) throw new Error(`Range out of bound ${pattern}`);
    const values = [];
    for (let i = start; i < end; i += 1) {
      if (i % increment === 0) values.push(i);
    }
    return values.join(' ');
  }],
]);

const parse = (cronArguments) => {
  const toReturn = {};

  Object.keys(cronArguments).map((type) => {
    const pattern = cronArguments[type];
    const { start, end } = CRON_EXPRESSION_RANGE[type];

    toReturn[type] = CRON_TYPES_PARSER({ pattern, start, end });
  });

  return toReturn;
};

module.exports = { parse };
